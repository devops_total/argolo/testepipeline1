#!/bin/bash

echo "Digite um numero"
read num1
echo "Digite outro numero"
read num2

echo "Escolha a operação desejada"
echo " 1 - Soma "
echo " 2 - Subtração "
echo " 3 - Multiplicação "
echo " 4 - Divisão "
echo " 5 - Media "
read equacao

if [ $equacao -eq 1 ]; then 
    resultado=$((num1 + num2))
    echo "O resultado é: $resultado"
    
elif [ $equacao -eq 2 ]; then 
    resultado=$((num1 - num2))
    echo "O resultado é: $resultado"
    
elif [ $equacao -eq 3 ]; then 
    resultado=$((num1 * num2))
    echo "O resultado é: $resultado"
    
elif [ $equacao -eq 4 ]; then 
    resultado=$((num1 / num2))
    echo "O resultado é: $resultado"
    
elif [ $equacao -eq 5 ]; then 
    resultado=$(( (num1 + num2) / 2 ))
    echo "O resultado é: $resultado"
    
else
    echo "Operação não existe"
fi